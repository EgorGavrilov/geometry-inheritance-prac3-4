﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance.Geometry
{
    public interface IVisitor
    {
        double Visit(Ball ball);
        double Visit(Cube cube);
        double Visit(Cyllinder cyllinder);
    }

    public class VolumeVisitor : IVisitor
    {
        public double Visit(Ball ball)
        {
            return 4 * Math.PI * Math.Pow(ball.Radius, 3) / 3;
        }

        public double Visit(Cube cube)
        {
            return Math.Pow(cube.Size, 3);
        }

        public double Visit(Cyllinder cyllinder)
        {
            return Math.PI * Math.Pow(cyllinder.Radius, 2) * cyllinder.Height;
        }
    }

    public abstract class Body
    {
        public abstract double Accept(IVisitor visitor);
    }

    public static class BodyExtensions
    {
        static VolumeVisitor volumeVisitor = new VolumeVisitor();
        public static double GetVolume(this Body body)
        {
            return body.Accept(volumeVisitor);
        }
    }

    public class Ball : Body
    {
        public double Radius { get; set; }
        public override double Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }

    public class Cube : Body
    {
        public double Size { get; set; }
        public override double Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }

    public class Cyllinder : Body
    {
        public double Height { get; set; }
        public double Radius { get; set; }
        public override double Accept(IVisitor visitor)
        {
             return visitor.Visit(this);
        }
    }
}
